#!/usr/bin/env python
import unittest
from .test_credMethods import TestCredMethods
from .test_vaultMethods import TestVaultMethods

myCredTest = TestCredMethods()
myVaultTest = TestVaultMethods()


if __name__ == '__main__':
    unittest.main()