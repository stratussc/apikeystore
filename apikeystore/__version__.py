

__title__ = "apikeystore"
__version__ = '0.0.10a1'
__author__ = 'Eugene Wright'
__author_email__ = "eugene@ewright3.com"
__license__ = "MIT"
__copyright__ = 'Copyright 2020 Eugene Wright'
__homepage__ = "https://bitbucket.org/ewright3/apikeystore/"