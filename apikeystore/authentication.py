#!/usr/bin/env python

class basic(object):
    '''This Class handles the basic authentication process'''

class oauth(object):
    '''This class handles the OAUTHv1 authentication process'''

class oauth2(object):
    '''This class handles OAUTHv2 authentication process'''

class saml(object):
    '''This class handles SAML authentication process'''

class custom(object):
    '''This class handles custom autthentication processes'''
